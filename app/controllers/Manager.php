<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Manager extends Controller
{
  private $url = BASEURL . 'Manager/';

  
    // method default
  public function index()
  {
    $data['siswa'] = $this->model('Model_siswa')->distribuSiswa();
    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/index',$data);
    $this->view('template/footer');
  }
  
  // method daftar siswa
  public function siswa($pn=1){
    $data['pn'] = $pn;
    $data['url'] = $this->url . "siswa";
    $data['siswa']=$this->model('Model_siswa')->tampil($pn);
    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/dfSiswa',$data);
    $this->view('template/footer');
  }

  // method formlir siswa
  public function frsiswa($idx=""){
      if($idx == ""){
          $data['siswa'] = [
            'namaLengkap'=>'',
            'jenisKelamin'=>'',
            'namaAyah'=>'',
            'nomorHPSiswa'=>'',
            'nomorHPBapak'=>'',
            'nis'=>''
          ];
          $data['action'] = $this->url . "addSiswa";
      }else{
          $data['siswa'] = $this->model('Model_siswa')->detail($idx);
          $data['action'] = $this->url . "chgSiswa";
      }

      $this->view('template/header');
      $this->view('manager/mgrNav');
      $this->view('manager/frsiswa',$data);
      $this->view('template/footer');
  }
  
  // method tambah siswa
  public function addSiswa(){
    if($this->model('Model_siswa')->tambah($_POST) > 0 ){
        Alert::setAlert('berhasil ditambahkan','Data siswa','success');
    }else{          
        Alert::setAlert('gagal ditambahkan','Data siswa','danger');
    }
    header("Location:" . $this->url . "siswa");
  }

  // method ubah siswa
  public function chgSiswa(){
    if($this->model('Model_siswa')->ngubah($_POST) > 0 ){
        Alert::setAlert('berhasil dimutakhirkan','Data siswa','success');
    }else{          
        Alert::setAlert('gagal dimutakhirkan','Data siswa','danger');
    }
    header("Location:" . $this->url . "siswa");
  }
  // method hapus siswa
  public function droppingout(){
    if($this->model('Model_siswa')->sampah($_POST) > 0 ){
        echo "1";
    }else{          
        echo "0";
    }
  }

  // method cari siswa 
  public function siswabyname(){
    $data = $this->model('Model_siswa')->siswabyname($_POST);
    echo json_encode($data,JSON_PRETTY_PRINT);
  }


  // method daftar Guru
  public function guru($pn=1)
  {
    $data['guru']=$this->model('Model_gurukaryawan')->tampil($pn);
    $data['pn'] = $pn;
    $data['url'] = $this->url . "guru/";
    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/dfGuru',$data);
    $this->view('template/footer');
  }

  // method daftar Formulir Guru
  public function frguru($idx="")
  {
    if( $idx == "" ){
        $data['guru'] = [
            'niy'=>'',
            'namaLengkap'=>'',
            'jenisKelamin'=>'',
            'posisi'=>'',
            'nomorHP'=>''
        ];
        $data['action'] = BASEURL."Manager/addGurukary";
    }else{
        $data['guru'] = $this->model('Model_gurukaryawan')->detail($idx);
        $data['action'] = BASEURL."Manager/chgGurukary";
    }
    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/frguru',$data);
    $this->view('template/footer');
  }

  // method tambah guru karyawan
  public function addGurukary(){
    if($this->model('Model_gurukaryawan')->tambah($_POST) > 0){
      Alert::setAlert('berhasil ditambahkan' , 'Data guru/karyawan' , 'success');
    }else{          
      Alert::setAlert('gagal ditambahkan' , 'Data guru/karyawan' , 'danger');
    }
    header("Location:" . BASEURL . "Manager/guru");
  }
    
  public function chgGurukary(){
    if($this->model('Model_gurukaryawan')->ngubah($_POST) > 0){
      Alert::setAlert('berhasil dimutakhirkan' , 'Data guru/karyawan' , 'success');
    }else{          
      Alert::setAlert('gagal dimutakhirkan' , 'Data guru/karyawan' , 'danger');
    }
    header("Location:" . BASEURL . "Manager/guru");
  }

  // method hapus guru karyawan
  public function firing(){
      if( $this->model('Model_gurukaryawan')->sampah($_POST) > 0 ){
          echo "1";
      }else{
          echo "0";
      }
  }

  // method default
  public function mapel()
  {
    $data['mapel']=$this->model('Model_mapel')->tampil(1);
    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/dfMapel',$data);
    $this->view('template/footer');
  }

  // method form mapel
  public function frmapel($idx=""){
    // 
    if($idx == ""){
      $data['mapel'] = [
        'proli'=>'' , 
        'initMapel'=>'', 
        'namaMapel'=>'' , 
        'mapelID'=>''
      ];
      $data['action'] = $this->url . "addMapel";
    }else{
      $data['mapel'] = $this->model('Model_mapel')->detail($idx);
      $data['action'] = $this->url . "chgMapel";
    }

    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/frmapel',$data);
    $this->view('template/footer');
  }

  public function addMapel(){
    if($this->model('Model_mapel')->tambah($_POST) > 0 ){
      Alert::setAlert('berhasil ditambahkan','Data mata pelajaran','success');
    }else{
      Alert::setAlert('gagal ditambahkan','Data mata pelajaran','danger');
    }

    header("Location: " . $this->url . "mapel");
  }

  public function chgMapel(){

    if($this->model('Model_mapel')->ngubah($_POST) > 0 ){
      Alert::setAlert('berhasil dimutakhirkan','Data mata pelajaran','success');
    }else{
      Alert::setAlert('gagal dimutakhirkan','Data mata pelajaran','danger');
    }
    
    header("Location: " . $this->url . "mapel");
  }

  public function rmvMapel(){

    if($this->model('Model_mapel')->sampah($_POST) > 0 ){
      echo "1";
    }else{
      echo "0";
    }

  }

  // method default
  public function kelas()
  {
    $data['kelas'] = $this->model('Model_kelas')->tampil(1);
    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/dfKelas',$data);
    $this->view('template/footer');
  }

  // method aktivasi kelas
  public function setActivity(){
    if($this->model('Model_kelas')->setActivity($_POST) > 0 ){
      echo "1";
    }else{
      echo "0";
    }
  }

  // method kontrak mengajar
  public function tgsajar($pn = 1)
  {    
    $data['pn'] = $pn;
    $data['url'] = $this->url . 'tgsajar';
    $data['kontrak'] = $this->model('Model_gmpk')->tampil($pn);
    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/gmpk',$data);
    $this->view('template/footer');
  }

  // method formulir kontrak mengajar
  public function frAddKontrak(){
    $data['mapel'] = $this->model('Model_mapel')->tampil(1);
    $data['guru'] = $this->model('Model_gurukaryawan')->tampilGuru(1);
    $data['kelas'] = $this->model('Model_kelas')->kelasAktif(1);

    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/frGmpkAdd',$data);
    $this->view('template/footer');

  }

  public function setKontrak(){
    
    $result=0;
    foreach($_POST['kelas'] as $i => $kelas){
      $data = ['tapel'=>$_POST['tapel'] , 'niy'=>$_POST['niy'] , 'mapelID'=>$_POST['mapelID'] , 'kelasId'=>$kelas];
      if( $this->model('Model_gmpk')->tambah($data) > 0 ){
      }
    }
    if($result = COUNT($_POST['kelas'])){
      Alert::setAlert('berhasil ditambahkan' , 'Data kontrak mengajar', 'success' );
    }else{
      Alert::setAlert('gagal ditambahkan' , 'Data kontrak mengajar', 'danger' );
    }
    header("Location:" . $this->url ."tgsajar" );
  }

  public function frChgKontrak($idx){
    $data['kontrak'] = $this->model('Model_gmpk')->detail($idx);
    $data['mapel'] = $this->model('Model_mapel')->tampil(1);
    $data['guru'] = $this->model('Model_gurukaryawan')->tampilGuru(1);
    $data['kelas'] = $this->model('Model_kelas')->kelasAktif(1);
    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/frGmpkChg',$data);
    $this->view('template/footer'); 
  }
  
  public function chgKontrak(){
    if($this->model('Model_gmpk')->ngubah($_POST) > 0 ){
      Alert::setAlert('berhasil dimutakhirkan' , 'Data kontrak mengajar', 'success' );
    }else{
      Alert::setAlert('gagal dimutakhirkan' , 'Data kontrak mengajar', 'danger' );
    }
    header("Location:" . $this->url ."tgsajar" );
  }

  // method hapus kontrak
  public function putusKontrak(){
    if($this->model('Model_gmpk')->sampah($_POST) > 0 ){ echo "1"; }else{ echo "0"; }
  }

  // method default siswa masing-masing kelas
  public function siswakelas($kelas="10TKR01"){

    $data['kelas'] = $this->model('Model_kelas')->kelasAktif(1);
    $data['kelasiswa'] = $this->model('Model_kelas')->detail($kelas);
    $data['siswa'] = $this->model('Model_siswakelas')->detail($kelas);

    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/dfSiswaKelas',$data);
    $this->view('template/footer');

  }

  // method default siswa masing-masing kelas
  public function sysuser($uid=""){

    $this->view('template/header');
    $this->view('manager/mgrNav');
    $this->view('manager/sysuser',$data);
    $this->view('template/footer');

  }

  public function userFind($uid){
    $user = $this->model('Model_user')->detail($uid);
    if($user['userType'] == 'nis'){
      $udata = $this->model('Model_siswa')->detail($uid);
    }else{      
      $udata = $this->model('Model_gurukaryawan')->detail($uid);
    }
    $userDetail = ['user'=>$user,'detail'=>$udata];
    $userDetail = [
      'userID'=>$uid,
      'namaLengkap'=>$udata['namaLengkap'],
      'type'=>$user['userType']
    ];
    echo json_encode($userDetail, JSON_PRETTY_PRINT);
  }

  public function setUser(){
    // Array ( [umodus] => gantos [usertype] => nis [userID] => 20-2-1200 [userName] => 2021200 [userAuth] => bakso%631 )
    // userID , userName , userType , userAuth , userOute
    $userRoute = $_POST['usertype'] == 'nis' ? 'Siswa' : 'Guru';
    $data = ['userID'=>$_POST['userID'] , 'userName'=>$_POST['userName'] , 'userAuth' => $_POST['userAuth'] , 'userOute' => $userRoute ];
    if($_POST['umodus'] == 'gantos'){
      if( $this->model('Model_user')->ngubah($data) > 0 ){
        Alert::setAlert('berhasil diupdate','Data User','success');
      }else{        
        Alert::setAlert('gagal diupdate','Data User','danger');
      }
    }else{
      if( $this->model('Model_user')->tambah($data) > 0 ){
        Alert::setAlert('berhasil disimpan','Data User','success');
      }else{        
        Alert::setAlert('gagal disimpan','Data User','danger');
      }      
    }
    header("Location:" . BASEURL .'Manager/sysuser');
  }



}

// Template methode
// public function index()
// {
//   $this->view('template/header');
//   $this->view('manager/mgrNav');
//   $this->view('manager/_template');
//   $this->view('template/footer');
// }