<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Test extends Controller
{
  // method default
  public function index()
  {
    $data['title'] = "test page";
    $this->view('template/header',$data);
    $this->view('forms/test-header');
    $this->view('forms/mapel');
    $this->view('forms/test-footer');
    $this->view('template/footer');
  }
}
