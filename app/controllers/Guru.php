<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Guru extends Controller
{
  // method default
  private $niy;

  public function __construct(){
      $this->niy=$_SESSION['UID'];
      // $_SESSION['niy'] = $this->niy;
  }
  public function index()
  {
    $data['kontrak'] = $this->model('Model_gmpk')->kontrakGuru($this->niy);
    $data['guru'] = $this->model('Model_gurukaryawan')->detail($this->niy);
    $data['jurnal'] = $this->model('Model_jurnal')->tampil($this->niy);
    $this->view('template/header');
    $this->view('guru/zguruNav',$data);
    $this->view('guru/index',$data);
    $this->view('template/footer');
  }

  public function jurnal($id,$jid=""){

    if( $jid != "" ){
      $data['jpbm'] = $this->model('Model_jurnal')->detail($jid);
      $data['fmod'] = 'chg';
    }else{
      // topic,materiPbm,materiEvaluasi,tugasMandiri
      $data['jpbm']['topic'] = null;
      $data['jpbm']['materiPbm'] = null;
      $data['jpbm']['maeriEvaluasi'] = null;
      $data['jpbm']['tugasMandiri'] = null;
      $data['fmod'] = 'new';
    }

    $data['kontrak'] = $this->model('Model_gmpk')->detail($id);
    $data['jurnal'] = $this->model('Model_jurnal')->jurnalKelasGuru($id);
    $data['jurnalIDbaru'] = $this->model('Model_jurnal')->jpbmIDbaru($id);

    $this->view('template/header');
    $this->view('guru/zguruNav',$data);
    $this->view('guru/jurnal',$data);
    $this->view('template/footer');
  }

  //  method jurnal baru
  public function jurnalbaru(){
    list($tanggal,$kontrakID,$urut) = explode("-",$_POST['jpbmID']);
    if($_POST['mod'] == 'new'){

      if($this->model('Model_jurnal')->tambah($_POST) > 0 ){
          Alert::setAlert('berhasil dicatat' , 'Jurnal KBM' , 'success');
        }else{
          Alert::setAlert('gagal dicatat' , 'Jurnal KBM' , 'danger');
      }

    }else{
      if($this->model('Model_jurnal')->ngubah($_POST) > 0 ){
        Alert::setAlert('berhasil diubah' , 'Jurnal KBM' , 'success');
      }else{
        Alert::setAlert('gagal diubah' , 'Jurnal KBM' , 'danger');
      }
    }

      header("Location:" . BASEURL ."Guru/jurnal/{$kontrakID}");
  }

  public function addNote(){
    $_POST['jpbmID'] = trim($_POST['jpbmID']);
    // var_dump($_POST['jpbmID']);
    if($this->model('Model_jurnal')->tambahNote($_POST) > 0 ){
      Alert::setAlert('berhasil ditambahkan' , 'Catatan jurnal KBM' , 'success');
    }else{
      Alert::setAlert('gagal ditambahkan' , 'Catatan jurnal KBM' , 'danger');
    }
    header("Location:" . BASEURL . "Guru/jurnal/{$_POST['gmpkID']}");
  }

  // method isi presensi jurnal pbm
  public function presensijurnal($jpbmID){
    $data['jurnal'] = $this->model('Model_jurnal')->detail($jpbmID);
    $data['presensi'] = $this->model('Model_presensiPbm')->chekPresensi($jpbmID);
    $data['dhkelas'] = $this->model('Model_presensiPbm')->dhkelas( $data['jurnal']['kelasId']);
    if($data['dhkelas'] > 0) {
      $presiswa = $this->model('Model_presensiPbm')->preskelas( $data['jurnal']['kelasId'] );
      // print_r($presiswa);
      foreach($presiswa as $pres){
         $nis=$pres['nis']; $ket = $pres['keterangan'];
         $data['preskelas'][$nis] = $ket;
      }
    }else{
      $data['preskelas'] = NULL;
    }
    $this->view('template/header');
    $this->view('guru/zguruNav');
    if( $data['presensi']['presensi'] == 0 ){
      $data['siswa']  = $this->model('Model_siswakelas')->kelasPresensi($jpbmID);
      $this->view('guru/frPresensikbm',$data);
    }else{
      $data['siswa'] = $this->model('Model_presensiPbm')->attendance($jpbmID);
      $this->view('guru/dfPresensikbm',$data);
    }
    $this->view('template/footer');
  }

  // method catat presensi pbm
  public function setPresensi($kelasID){
    $dhkelas = $this->model('Model_presensiPbm')->dhkelas($kelasID);
    $success = 0; $dhk=0;
    
    foreach($_POST as $presID => $presence ){
      $data = ['presenceID'=>$presID , 'keterangan'=>$presence];
      $dhid = substr($presID,0,6) . '_' . substr($presID,-9);
      $dhdata = ['kelasID'=>$kelasID , 'presenceID' =>$dhid , 'keterangan'=>$presence];
      // echo $dhid . ' - ' . $presence ."<br/>";
      
      if( $dhkelas == 0 ){
        
        // Insert Presensi PBM dan Presensi Harian Kelas
        if($this->model('Model_presensiPbm')->setPresensi($data) > 0 && $this->model('Model_presensiPbm')->setDhkelas($dhdata) > 0 ){
          $success +=1;
        }else{
          $success +=0;
        }

      }else{

        // Insert Presensi PBM saja
        if($this->model('Model_presensiPbm')->setPresensi($data) > 0 ){
          $success +=1;
        }else{
          $success +=0;
        }
        

      }
      $gmpkID=substr($presID,7,4);
    }
    
    if( $success >= count($_POST) ){
      Alert::setAlert('berhasil dicatat','Presensi PBM' , 'success');
    }else{
      Alert::setAlert('gagal dicatat','Presensi PBM' , 'danger');
    }
    
    header("Location:" . BASEURL ."Guru/jurnal/{$gmpkID}");
  }

}
