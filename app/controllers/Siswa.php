<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Siswa extends Controller
{
    private $nis;
    //   session daruroh
  public function __construct(){
      // $_SESSION['nis']  = "20-1-1123";
      $this->nis = $_SESSION['UID'];
  }
  
    // method default
  public function index()
  {
    $data['siswa'] = $this->model('Model_siswa')->detail($this->nis);
    $data['kelas'] = $this->model('Model_kelas')->kelasSiswa($this->nis);
    $data['jurnal']= $this->model('Model_jurnal')->jurnalSiswa($data['kelas']['kelasID']);
    $this->view('template/header');
    $this->view('siswa/pageBar');
    $this->view('siswa/index',$data);
    $this->view('template/footer');
  }
}
