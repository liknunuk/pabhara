<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Monitor extends Controller
{
  // method default
  public function index()
  {
    $data['jurnal'] = $this->model('Model_jurnal')->jurnalTerkini();
    $data['presensi'] = $this->model('Model_presensiPbm')->okupansi();
    $this->view('template/header');
    $this->view('home/monitor',$data);
    $this->view('template/footer');
  }
}
