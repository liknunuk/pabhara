<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $this->view('template/header');
    $this->view('home/index');
    $this->view('template/footer');
  }

  public function siswaKelas($id){
    $data = $this->model('Model_siswakelas')->detail($id);
    echo json_encode($data,JSON_PRETTY_PRINT);
  }

  public function userAuth(){
    $user =  $this->model('Model_user')->loginto($_POST);

    // if( $user['userType'] == 'niy'){
    //   $userData = $this->cariGuru($user['userID']);
    // }elseif( $user['userType'] == 'nis'){
    //   $userData = $this->cariSiswa($user['userID']);
    // }else{
    //   header("Location:" . BASEURL);
    // }
    
    // $_SESSION['nama'] = $userData['namaLengkap'];
    $_SESSION['UID'] = $user['userID'];
    $_SESSION['Route'] = $user['userOute'];
    header("Location: " . BASEURL . $user['userOute']);
    
  }

  private function cariGuru($id){
    return $this->model('Model_gurukaryawan')->detail($id);
  }
  
  private function cariSiswa($id){
    return $this->model('Model_siswa')->detail($id);
  }

  public function logout(){
    session_unset();
    session_destroy();
    header("Location:" . BASEURL);
  }
}
