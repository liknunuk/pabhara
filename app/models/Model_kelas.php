<?php
class Model_kelas
{
    // tingkat , proli , ruang , statuskelas , kelasID
    private $table = "kelas";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET tingkat=:tingkat , proli=:proli , ruang=:ruang , statuskelas=:statuskelas , kelasID=:kelasID ";
        $this->db->query($sql);
        $this->db->bind('tingkat',$data['tingkat']);
        $this->db->bind('proli',$data['proli']);
        $this->db->bind('ruang',$data['ruang']);
        $this->db->bind('statuskelas',$data['statuskelas']);
        $this->db->bind('kelasID',$data['kelasID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET WHERE tingkat=:tingkat , proli=:proli , ruang=:ruang , statuskelas=:statuskelas WHERE kelasID=:kelasID ";
        $this->db->query($sql);
        $this->db->bind('tingkat',$data['tingkat']);
        $this->db->bind('proli',$data['proli']);
        $this->db->bind('ruang',$data['ruang']);
        $this->db->bind('statuskelas',$data['statuskelas']);
        $this->db->bind('kelasID',$data['kelasID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE kelasID=:kelasID";
        $this->db->query($sql);
        $this->db->bind('kelasID',$data['kelasID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY statuskelas, kelasID LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE kelasID=:key ";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    // DISPLAY MULTIPLE
    public function kelasAktif($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table WHERE statuskelas = 'Aktif' ORDER BY kelasID LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function kelasSiswa($data){
        $sql = "SELECT * FROM kelasSiswa WHERE nis=:data LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('data',$data);
        return $this->db->resultOne();
    }

}

// QUERY TEMPLATE
// public function something($data){
//     $sql = "";
//     $this->db->query($sql);
//     $this->db->bind('xxx',$data['xxx']);
//     $this->db->bind('xxx',$xxx);
//     return $this->db->resultSet();
// }