<?php
class Model_gmpk
{
    private $table = "guruMapelKelas";
    private $db;
    // kolom: tapel , niy , mapelID , kelasId, gmpkID
    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET tapel=:tapel , niy=:niy , mapelID=:mapelID , kelasId=:kelasId ";
        $this->db->query($sql);
        $this->db->bind('tapel',$data['tapel']);
        $this->db->bind('niy',$data['niy']);
        $this->db->bind('mapelID',$data['mapelID']);
        $this->db->bind('kelasId',$data['kelasId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET tapel=:tapel , niy=:niy , mapelID=:mapelID , kelasId=:kelasId WHERE gmpkID=:gmpkID";
        $this->db->query($sql);
        $this->db->bind('tapel',$data['tapel']);
        $this->db->bind('niy',$data['niy']);
        $this->db->bind('mapelID',$data['mapelID']);
        $this->db->bind('kelasId',$data['kelasID']);
        $this->db->bind('gmpkID',$data['gmpkID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE gmpkID=:gmpkID";
        $this->db->query($sql);
        $this->db->bind('gmpkID',$data['gmpkID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT guruMapelKelas.* , gurukaryawan.namaLengkap , mapel.namaMapel , kelas.* FROM guruMapelKelas, kelas, gurukaryawan, mapel WHERE gurukaryawan.niy = guruMapelKelas.niy && kelas.kelasID = guruMapelKelas.kelasId && mapel.mapelID = guruMapelKelas.mapelID ORDER BY gurukaryawan.namaLengkap , mapel.namaMapel , guruMapelKelas.kelasID LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT guruMapelKelas.gmpkID , mapel.namaMapel , kelas.tingkat , kelas.proli , kelas.ruang FROM guruMapelKelas , mapel, kelas WHERE mapel.mapelID = guruMapelKelas.mapelID && kelas.kelasID = guruMapelKelas.kelasId && gmpkID=:key ";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function kontrakGuru($niy){
        $sql = "SELECT guruMapelKelas.gmpkID , mapel.namaMapel , kelas.tingkat , kelas.proli , kelas.ruang FROM guruMapelKelas , mapel, kelas WHERE mapel.mapelID = guruMapelKelas.mapelID && kelas.kelasID = guruMapelKelas.kelasId && niy=:niy && tapel=:tapel";

        $this->db->query($sql);
        $this->db->bind('niy',$niy);
        $this->db->bind('tapel',tapel);
        return $this->db->resultSet();
    }

}

// QUERY TEMPLATE
// public function something($data){
//     $sql = "";
//     $this->db->query($sql);
//     $this->db->bind('xxx',$data['xxx']);
//     $this->db->bind('xxx',$xxx);
//     return $this->db->resultSet();
// }