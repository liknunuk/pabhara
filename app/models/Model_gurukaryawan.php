<?php
class Model_gurukaryawan
{
    private $table = "gurukaryawan";
    // kolom -- niy , namaLengkap , jenisKelamin , posisi , nomorHP
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET  namaLengkap=:namaLengkap , jenisKelamin=:jenisKelamin , posisi=:posisi , nomorHP=:nomorHP , niy=:niy ";
        $this->db->query($sql);
        $this->db->bind('namaLengkap',$data['namaLengkap']);
        $this->db->bind('jenisKelamin',$data['jenisKelamin']);
        $this->db->bind('posisi',$data['posisi']);
        $this->db->bind('nomorHP',$data['nomorHP']);
        $this->db->bind('niy',$data['niy']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET namaLengkap=:namaLengkap , jenisKelamin=:jenisKelamin , posisi=:posisi , nomorHP=:nomorHP WHERE niy=:niy LIMIT 1 ";
        $this->db->query($sql);
        $this->db->bind('namaLengkap',$data['namaLengkap']);
        $this->db->bind('jenisKelamin',$data['jenisKelamin']);
        $this->db->bind('posisi',$data['posisi']);
        $this->db->bind('nomorHP',$data['nomorHP']);
        $this->db->bind('niy',$data['niy']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE niy=:niy";
        $this->db->query($sql);
        $this->db->bind('niy',$data['niy']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY posisi , niy LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE niy=:key ";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultOne();
    }
    
    // Custom Queries

    public function tampilGuru($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table WHERE posisi='Guru' ORDER BY namaLengkap LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

}

// QUERY TEMPLATE
