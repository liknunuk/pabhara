<?php
class Model_jurnal
{
    private $table = "jurnalPbm";
    private $db;
    // Kolom: semester , waktuCatat , topic , catatan , jpbmID

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET jpbmID=:jpbmID , semester=:semester , topic=:topic , materiPbm=:materiPbm , materiEvaluasi=:materiEvaluasi , tugasMandiri=:tugasMandiri";
        $this->db->query($sql);
        $this->db->bind('jpbmID',$data['jpbmID']);
        $this->db->bind('semester',$data['semester']);
        $this->db->bind('topic',$data['topic']);
        $this->db->bind('materiPbm',$data['materiPbm']);
        $this->db->bind('materiEvaluasi',$data['materiEvaluasi']);
        $this->db->bind('tugasMandiri',$data['tugasMandiri']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET topic=:topic , materiPbm=:materiPbm , materiEvaluasi=:materiEvaluasi , tugasMandiri=:tugasMandiri WHERE jpbmID=:jpbmID";
        $this->db->query($sql);
        $this->db->bind('topic',$data['topic']);
        $this->db->bind('materiPbm',$data['materiPbm']);
        $this->db->bind('materiEvaluasi',$data['materiEvaluasi']);
        $this->db->bind('tugasMandiri',$data['tugasMandiri']);
        $this->db->bind('jpbmID',$data['jpbmID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE ";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($niy){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT jurnalPbm.* ,  CONCAT(kelas.tingkat,' ',kelas.proli,' ',kelas.ruang) kelas , mapel.namaMapel FROM jurnalPbm , guruMapelKelas , kelas , mapel WHERE guruMapelKelas.gmpkID = MID(jpbmID , 8, 4) && kelas.kelasID = guruMapelKelas.kelasId && mapel.mapelID = guruMapelKelas.mapelID && guruMapelKelas.niy=:niy ORDER BY waktuCatat DESC limit 60";
        $this->db->query($sql);
        $this->db->bind('niy',$niy);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($jpbmID){
        $sql = "SELECT jurnalPbm.jpbmID , jurnalPbm.semester , mapel.namaMapel , jurnalPbm.topic , jurnalPbm.materiPbm , jurnalPbm.materiEvaluasi , jurnalPbm.tugasMandiri , CONCAT(kelas.tingkat,' ',kelas.proli,' ',kelas.ruang) kelas , guruMapelKelas.kelasId FROM jurnalPbm , mapel , guruMapelKelas , kelas WHERE mapel.mapelID = guruMapelKelas.mapelID && guruMapelKelas.gmpkID = MID(jurnalPbm.jpbmID,8,4) && jurnalPbm.jpbmID=:jpbmID && kelas.kelasID = guruMapelKelas.kelasId;";
        $this->db->query($sql);
        $this->db->bind('jpbmID',$jpbmID);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function jurnalKelasGuru($data){
        $sql = "SELECT jpbmID , semester , waktuCatat , topic , materiPbm , materiEvaluasi, tugasMandiri , catatan FROM $this->table WHERE jpbmID LIKE :data ";
        $this->db->query($sql);
        $this->db->bind('data',"%-{$data}-%");
        return $this->db->resultSet();
    }
    
    // method nomor jurnal baru
    public function jpbmIDbaru($id){
        $identifier=date('ymd').'-'.$id.'-';
        $sql = "SELECT CONCAT(:identifier1,COUNT(jpbmID)+1) jpbmID FROM jurnalPbm WHERE jpbmID LIKE :identifier2";
        $this->db->query($sql);
        $this->db->bind('identifier1',$identifier);
        $this->db->bind('identifier2',$identifier."%");
        $result = $this->db->resultOne();
        return $result['jpbmID'];
    }

    // methdo tambahNOte
    public function tambahNote($data){
        $sql = "UPDATE $this->table SET catatan=:catatan WHERE jpbmID=:jpbmID";
        $this->db->query($sql);
        $this->db->bind('catatan',$data['catatan']);
        $this->db->bind('jpbmID',$data['jpbmID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // QUERY TEMPLATE
    public function something($data){
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->bind('xxx',$xxx);
        $this->db->execute();
        return $this->db->resultSet();
        return $this->db->rowCount();
    }

    // method jurnal hari ini
    public function jurnalTerkini(){
        $today = date('Y-m-d');
        $sql = "SELECT jurnalPbm.jpbmID , jurnalPbm.waktuCatat , jurnalPbm.topic , mapel.namaMapel , gurukaryawan.namaLengkap , CONCAT(kelas.tingkat,' ',kelas.proli,' ',kelas.ruang) kelas FROM jurnalPbm , mapel , gurukaryawan , guruMapelKelas , kelas WHERE jurnalPbm.waktuCatat LIKE :today && guruMapelKelas.gmpkID = MID(jurnalPbm.jpbmID , 8, 4) && mapel.mapelID = guruMapelKelas.mapelID && gurukaryawan.niy = guruMapelKelas.niy && kelas.kelasID = guruMapelKelas.kelasId" ;
        
        $this->db->query($sql);
        $this->db->bind('today',$today.'%');
        return $this->db->resultSet();
    }

    public function jurnalSiswa($data){
        $sql = "SELECT jurnalPbm.topic, jurnalPbm.materiPbm , jurnalPbm.materiEvaluasi , jurnalPbm.tugasMandiri , gurukaryawan.namaLengkap , guruMapelKelas.kelasId , mapel.namaMapel FROM jurnalPbm , gurukaryawan , guruMapelKelas , mapel WHERE guruMapelKelas.gmpkID = MID(jurnalPbm.jpbmID , 8 , 4 ) && gurukaryawan.niy = guruMapelKelas.niy && mapel.mapelID = guruMapelKelas.mapelID && guruMapelKelas.kelasId = :data ORDER BY jurnalPbm.jpbmID DESC LIMIT " . rows;
        $this->db->query($sql);
        $this->db->bind('data',$data);
        return $this->db->resultSet();
    }

}

// QUERY TEMPLATE
// public function something($data){
//     $sql = "";
//     $this->db->query($sql);
//     $this->db->bind('xxx',$data['xxx']);
//     $this->db->bind('xxx',$xxx);
//     $this->db->execute();
//     return $this->db->resultSet();
//     return $this->db->rowCount();
// }