<?php
class Model_siswa
{
    // Kolom : namaLengkap,jenisKelamin,namaAyah,nomorHPSiswa,nomorHPBapak,nis
    private $table = "siswa";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET namaLengkap=:namaLengkap , jenisKelamin=:jenisKelamin , namaAyah=:namaAyah , nomorHPSiswa=:nomorHPSiswa , nomorHPBapak=:nomorHPBapak , nis=:nis";
        $this->db->query($sql);
        $this->db->bind('namaLengkap',$data['namaLengkap']);
        $this->db->bind('jenisKelamin',$data['jenisKelamin']);
        $this->db->bind('namaAyah',$data['namaAyah']);
        $this->db->bind('nomorHPSiswa',$data['nomorHPSiswa']);
        $this->db->bind('nomorHPBapak',$data['nomorHPBapak']);
        $this->db->bind('nis',$data['nis']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET namaLengkap=:namaLengkap , jenisKelamin=:jenisKelamin , namaAyah=:namaAyah , nomorHPSiswa=:nomorHPSiswa , nomorHPBapak=:nomorHPBapak WHERE nis=:nis";
        $this->db->query($sql);
        $this->db->bind('namaLengkap',$data['namaLengkap']);
        $this->db->bind('jenisKelamin',$data['jenisKelamin']);
        $this->db->bind('namaAyah',$data['namaAyah']);
        $this->db->bind('nomorHPSiswa',$data['nomorHPSiswa']);
        $this->db->bind('nomorHPBapak',$data['nomorHPBapak']);
        $this->db->bind('nis',$data['nis']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE nis=:nis";
        $this->db->query($sql);
        $this->db->bind('nis',$data['nis']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table WHERE statusSiswa='aktif' ORDER BY nis LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE nis=:key ";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function siswabyname($data){
        $sql = "SELECT * FROM $this->table WHERE namaLengkap LIKE :nama";
        $this->db->query($sql);
        $this->db->bind('nama',"%{$data['nama']}%");
        return $this->db->resultSet();
    }
    
    public function distribuSiswa(){
        $sql = "SELECT tingkat , proli , jenisKelamin , kelasID , COUNT(jenisKelamin) jumlah FROM vwDistribuSiswa GROUP BY jenisKelamin,kelasID ORDER BY tingkat,proli";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

}

// QUERY TEMPLATE
// public function something($data){
//     $sql = "";
//     $this->db->query($sql);
//     $this->db->bind('xxx',$data['xxx']);
//     $this->db->bind('xxx',$xxx);
//     return $this->db->resultSet();
// }