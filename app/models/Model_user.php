<?php
class Model_user
{
    private $table = "sysuser";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    // column: userID , userName , userType , userAuth , userOute
    
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $userAuth = $this->setPassword($data['userName'],$data['userAuth']);
        $sql = "INSERT INTO $this->table SET userID=:userID , userName=:userName , userType=:userType , userAuth=:userAuth , userOute=:userOute ";
        $this->db->query($sql);
        $this->db->bind('userID',$data['userID']);
        $this->db->bind('userName',$data['userName']);
        $this->db->bind('userType',$data['userType']);
        $this->db->bind('userAuth',$userAuth);
        $this->db->bind('userOute',$data['userOute']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $userAuth = $this->setPassword($data['userName'],$data['userAuth']);
        $sql = "UPDATE $this->table SET userName=:userName , userType=:userType , userAuth=:userAuth , userOute=:userOute WHERE userID=:userID";
        $this->db->query($sql);
        $this->db->bind('userID',$data['userID']);
        $this->db->bind('userName',$data['userName']);
        $this->db->bind('userType',$data['userType']);
        $this->db->bind('userAuth',$userAuth);
        $this->db->bind('userOute',$data['userOute']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE userID=:userID";
        $this->db->query($sql);
        $this->db->bind('userID',$data['userID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        // $row = ($pn -1 ) * rows;
        // $sql = "SELECT * FROM $this->table ORDER BY LIMIT $row ," . rows;
        // $this->db->query($sql);
        // return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE userID=:key ";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function loginto($data){
        // Array ( [userName] => gty210022 [userAuth] => gurupabhara )
        $auth = $this->setPassword($data['userName'],$data['userAuth']);
        $sql = "SELECT * FROM sysuser WHERE userAuth =:auth LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('auth',$auth);
        return $this->db->resultOne();
    }

    private function setPassword($u,$p){
        // return md5($u.'+'.$p);
        return md5("{$u}+{$p}");
    }

}

// QUERY TEMPLATE
// public function something($data){
//     $sql = "";
//     $this->db->query($sql);
//     $this->db->bind('xxx',$data['xxx']);
//     $this->db->bind('xxx',$xxx);
//     return $this->db->resultSet();
//     $this->db->execute();
//     return $this->db->rowCount();
// }