<?php
class Model_mapel
{
    private $table = "mapel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    // Kolome: proli=:proli , initMapel=:initMapel, namaMapel=:namaMapel , mapelID=:mapelID
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET proli=:proli , initMapel=:initMapel, namaMapel=:namaMapel";
        $this->db->query($sql);
        $this->db->bind('proli',$data['proli']);
        $this->db->bind('initMapel',$data['initMapel']);
        $this->db->bind('namaMapel',$data['namaMapel']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET proli=:proli , initMapel=:initMapel, namaMapel=:namaMapel WHERE mapelID=:mapelID";
        $this->db->query($sql);
        $this->db->bind('proli',$data['proli']);
        $this->db->bind('initMapel',$data['initMapel']);
        $this->db->bind('namaMapel',$data['namaMapel']);
        $this->db->bind('mapelID',$data['mapelID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE mapelId=:mapelID";
        $this->db->query($sql);
        $this->db->bind('mapelID',$data['mapelID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY proli , namaMapel LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE mapelId=:key ";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function something($data){
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->bind('xxx',$xxx);
        return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
// public function something($data){
//     $sql = "";
//     $this->db->query($sql);
//     $this->db->bind('xxx',$data['xxx']);
//     $this->db->bind('xxx',$xxx);
//     return $this->db->resultSet();
// }