<?php
class Model_presensiPbm
{
    private $table = "presensiPbm";
    private $db;
    // kolom: presenceID , keterangan

    public function __construct()
    {
        $this->db = new Database();
    }

    // QUERY Insert Presensi PBM
    public function setPresensi($data){
        $sql = "INSERT INTO $this->table SET presenceID=:presenceID , keterangan=:keterangan";
        $this->db->query($sql);
        $this->db->bind('presenceID',$data['presenceID']);
        $this->db->bind('keterangan',$data['keterangan']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // method periksa kehadiran
    public function chekPresensi($jpbmID){
        $sql = "SELECT COUNT(presenceID) presensi FROM presensiPbm WHERE presenceID LIKE :jpbmID";
        $this->db->query($sql);
        $this->db->bind('jpbmID',$jpbmID."%");
        return $this->db->resultOne();
    }
    
    // method data kehadiran siswa pada pbm
    public function attendance($jpbmID){
        $sql = "SELECT presensiPbm.* , siswa.namaLengkap FROM presensiPbm,siswa WHERE presenceID LIKE :jpbmID && siswa.nis = RIGHT(presenceID,9) ORDER BY namaLengkap";
        $this->db->query($sql);
        $this->db->bind('jpbmID',$jpbmID."%");
        return $this->db->resultSet();
    }

    // Method Rekap Presensi Harian
    public function okupansi(){
        $today = date('ymd')."%";
        $sql = "SELECT keterangan , COUNT(keterangan) jumlah FROM presensiPbm WHERE presenceID LIKE :today GROUP BY keterangan ";
        $this->db->query($sql);
        $this->db->bind('today',$today);
        return $this->db->resultSet();
    }

    // QUERY Cek Presensi Harian kelas;
    public function dhkelas($kelasID){
        $today=date('ymd')."%";
        $sql = "SELECT COUNT(kelasID) presensi FROM presensiHarian WHERE kelasID=:kelasID && presenceID LIKE :today";
        $this->db->query($sql);
        $this->db->bind('kelasID',$kelasID);
        $this->db->bind('today',$today);
        // return $this->db->resultOne();
        $result = $this->db->resultOne();
        return $result['presensi'];
    }

    // method set presensi harian kelas
    public function setDhkelas($data){
        $sql = "INSERT INTO presensiHarian SET kelasID=:kelasID , presenceID=:presenceID , keterangan=:keterangan";
        $this->db->query($sql);
        $this->db->bind('kelasID',$data['kelasID']);
        $this->db->bind('presenceID',$data['presenceID']);
        $this->db->bind('keterangan',$data['keterangan']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // method siswa hadir kelas hari berjalan
    public function preskelas($kelasID){
        $tanggal = date('ymd')."%";
        $sql = "SELECT RIGHT(presenceID , 9) nis , keterangan FROM presensiHarian WHERE kelasID=:kelasID && presenceID LIKE :tanggal";
        $this->db->query($sql);
        $this->db->bind('kelasID',$kelasID);
        $this->db->bind('tanggal',$tanggal);
        return $this->db->resultSet();
    }
    

}

// QUERY TEMPLATE
// public function something($data){
//     $sql = "";
//     $this->db->query($sql);
//     $this->db->bind('xxx',$data['xxx']);
//     $this->db->bind('xxx',$xxx);
//     return $this->db->resultSet();
//     $this->db->execute();
//     return $this->db->rowCount();
// }