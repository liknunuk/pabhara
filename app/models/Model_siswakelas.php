<?php
class Model_siswakelas
{
    private $table = "kelasSiswa";
    private $db;
    // Kolom: tapel ,kelasID , nmAbsen , nis , klSiswaID(ai)
    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET tapel=:tapel ,kelasID=:kelasID , nmAbsen=:nmAbsen , nis=:nis ";
        $this->db->query($sql);
        $this->db->bind('tapel',$data['tapel']);
        $this->db->bind('kelasID',$data['kelasID']);
        $this->db->bind('nmAbsen',$data['nmAbsen']);
        $this->db->bind('nis',$data['nis']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET tapel=:tapel ,kelasID=:kelasID , nmAbsen=:nmAbsen , nis=:nis WHERE klSiswaID=:klSiswaID";
        $this->db->query($sql);
        $this->db->bind('tapel',$data['tapel']);
        $this->db->bind('kelasID',$data['kelasID']);
        $this->db->bind('nmAbsen',$data['nmAbsen']);
        $this->db->bind('nis',$data['nis']);
        $this->db->bind('klSiswaID',$data['klSiswaID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE klSiswaID=:klSiswaID";
        $this->db->query($sql);
        $this->db->bind('klSiswaID',$data['klSiswaID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY nmAbsen LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key="10TKR01"){
        $sql = "SELECT kelasSiswa.* , siswa.namaLengkap , siswa.jenisKelamin FROM $this->table , siswa WHERE kelasID=:key && siswa.nis = kelasSiswa.nis";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultSet();
    }

    // CUSTOMIZED QUERY //
    public function kelasPresensi($jpbmID){
        $sql="SELECT kelasSiswa.nis , kelasSiswa.nmAbsen , siswa.namaLengkap FROM kelasSiswa , siswa WHERE siswa.nis = kelasSiswa.nis && kelasID = (SELECT kelasId FROM guruMapelKelas WHERE gmpkID = (SELECT MID(jpbmID,8,4) FROM jurnalPbm WHERE jpbmID=:jpbmID))";
        $this->db->query($sql);
        $this->db->bind('jpbmID',$jpbmID);
        return $this->db->resultSet();
    }

}

// QUERY TEMPLATE
// public function something($data){
//     $sql = "";
//     $this->db->query($sql);
//     $this->db->bind('xxx',$data['xxx']);
//     $this->db->bind('xxx',$xxx);
//     return $this->db->resultSet();
//     $this->db->execute();
//     return $this->db->rowCount();
// }