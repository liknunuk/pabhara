
<form class='form-horizontal' action="<?=BASEURL;?>Manager/setKontrak" method="post">

<div class="form-group row">
  <label for="tapel" class="col-md-3">Tahun Pelajaran</label>
  <div class="col-md-9">
    <input type="text" name="tapel" id="tapel" class="form-control" required placeHolder="2020/2021">
  </div>
</div>

<div class="form-group row">
  <label for="mapelID" class="col-md-3">Mata Pelajaran</label>
  <div class="col-md-9">
    <select name="mapelID" id="mapelID" class="form-control">
      <option value="">Pilih Mata Pelajaran</option>
      <?php foreach($data['mapel'] as $mapel): ?>
      <option value="<?=$mapel['mapelID'];?>"><?=$mapel['namaMapel'];?></option>
      <?php endforeach; ?>
    </select>
  </div>
</div>

<div class="form-group row">
  <label for="niy" class="col-md-3">Guru Pengajar</label>
  <div class="col-md-9">
    <select type="text" name="niy" id="niy" class="form-control">
      <option value="">Pilih Guru</option>
      <?php foreach($data['guru'] as $guru): ?>
      <option value="<?=$guru['niy'];?>"><?=$guru['namaLengkap'];?></option>
      <?php endforeach; ?>
    </select>
  </div>
</div>

<div class="form-group row">
  <label for="xxx" class="col-md-3">Kelas Yang Diampu</label>
  <div class="col-md-9 d-flex flex-wrap">
  <?php foreach($data['kelas'] as $kelas): ?>
      <div class="custom-control custom-checkbox mx-2 mb-2">
          <input name="kelas[]" type="checkbox" class="custom-control-input" id="kls_<?=$kelas['kelasID'];?>" value="<?=$kelas['kelasID'];?>" >
          <label class="custom-control-label" for="kls_<?=$kelas['kelasID'];?>"><?=$kelas['tingkat'];?>-<?=$kelas['proli'];?>-<?=$kelas['ruang'];?></label>
      </div>
  <?php endforeach; ?>
  </div>
</div>

<div class="form-group d-flex justify-content-end">
  <input type="submit" value="Simpan" class="btn btn-primary">
</div>
</form>