<form class='form-horizontal' action="<?=$data['action'];?>" method="post">

  <div class="form-group row">
    <label for="niy" class="col-md-3">Nomor Induk Yayasan</label>
    <div class="col-md-9">
      <input type="text" name="niy" id="niy" class="form-control" required maxlength="12" placeholder="GTT-00-0000 / GTY-00-0000" value="<?=$data['guru']['niy'];?>" >
    </div>
  </div>

  <div class="form-group row">
    <label for="namaLengkap" class="col-md-3">Nama Lengkap</label>
    <div class="col-md-9">
      <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" required maxlength="40" placeholder="Nama Lengkap Guru / Karyawan" value="<?=$data['guru']['namaLengkap'];?>"  >
    </div>
  </div>

  <div class="form-group row">
    <label for="jenisKelamin" class="col-md-3">Jenis Kelamin</label>
    <div class="col-md-9">
    <select name="jenisKelamin" id="jenisKelamin" class="form-control">
        <option value="L" selected>Laki-laki</option>
        <option value="P">Perempuan</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="posisi" class="col-md-3">Guru/Karyawan</label>
    <div class="col-md-9">
      <select name="posisi" id="posisi" class="form-control">
        <option value="Guru" Selected >Guru</option>
        <option value="Karyawan">Karyawan</option>
        <option value="Rangkap">Rangkap Guru dan Karyawan</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="nomorHP" class="col-md-3">Nomor HP</label>
    <div class="col-md-9">
      <input type="text" name="nomorHP" id="nomorHP" class="form-control" placeholder="Nomor HP Aktif" value="<?=$data['guru']['nomorHP'];?>"  >
    </div>
  </div>
  
  <div class="form-group d-flex justify-content-end">
    <input type="submit" value="Simpan" class="btn btn-primary">
  </div>
</form>

<!-- 
    niy varchar(12) NOT NULL UNIQUE,
    namaLengkap varchar(40) NOT NULL,
    posisi enum('Guru','Karyawan') DEFAULT 'Guru',
    nomorHP varchar(12) NULL 
-->