
<form class='form-horizontal' action="<?=$data['action'];?>" method="post">

  <div class="form-group row">
    <label for="kelasID" class="col-md-3">Kelas ID</label>
    <div class="col-md-9">
      <?php if($data['kelas']['kelasID'] != "" ) { $ro = "readonly" ; }else{ $ro="" ;} ?>
      <input <?=$ro;?> type="text" name="kelasID" id="kelasID" class="form-control" required maxlength="12" placeholder="XXYYYZZ" value="<?=$data['kelas']['kelasID'];?>" >
    </div>
  </div>

  <div class="form-group row">
    <label for="tingkat" class="col-md-3">Tingkat</label>
    <div class="col-md-9">
      <select name="tingkat" id="tingkat" class="form-control">
      <option value="X">Tingkat X</option>
      <option value="XI">Tingkat XI</option>
      <option value="XII">Tingkat XII</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="proli" class="col-md-3">Program Keahlian</label>
    <div class="col-md-9">
    <select name="proli" id="proli" class="form-control">
        <option value="TKJ" selected>Teknik Komputer dan Jaringan</option>
        <option value="TKR">Teknik Kendaraan Ringan</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="ruang" class="col-md-3">Urut Ruang</label>
    <div class="col-lg-9">
        <input type="number" name="ruang" id="ruang" class="form-control" value="<?=$data['kelas']['ruang'];?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="statuskelas" class="col-md-3">Status Kelas</label>
    <div class="col-md-9">
      <select name="statuskelas" id="statuskelas" class="form-control">
        <option value="Inaktif" Selected >Tidak Aktir</option>
        <option value="Aktif">Masih Aktif</option>
      </select>
    </div>
  </div>
  
  <div class="form-group d-flex justify-content-end">
    <input type="submit" value="Simpan" class="btn btn-primary">
  </div>
</form>

<!-- 
 
-->