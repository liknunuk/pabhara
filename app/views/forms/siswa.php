<form class='form-horizontal' action="<?=$data['action'];?>" method="post">

  <div class="form-group row">
      <label for="nis" class="col-md-3">Nomor Induk Siswa</label>
    <div class="col-md-9">
      <?php if($data['siswa']['nis']!=""){$ro = "readonly"; }else{ $ro=""; } ?>
      <input type="text" name="nis" id="nis" class="form-control" required maxlegth='12' placeholder="XX-Y-ZZZZ" value="<?=$data['siswa']['nis'];?>" <?=$ro;?>> 
    </div>
  </div>

  <div class="form-group row">
    <label for="namaLengkap" class="col-md-3">Nama Lengkap</label>
    <div class="col-md-9">
      <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" required maxlength="40" placeholer="Nama Lengkap Siswa" value="<?=$data['siswa']['namaLengkap'];?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="jenisKelamin" class="col-md-3">Jenis Kelamin</label>
    <div class="col-md-9">
      <select name="jenisKelamin" id="jenisKelamin" class="form-control">
        <option value="L" selected>Laki-laki</option>
        <option value="P">Perempuan</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="namaAyah" class="col-md-3">Nama Ayah</label>
    <div class="col-md-9">
      <input type="text" name="namaAyah" id="namaAyah" class="form-control" maxlength="40" value="<?=$data['siswa']['namaAyah'];?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="nomorHPSiswa" class="col-md-3">Nomor HP Siswa</label>
    <div class="col-md-9">
      <input type="text" name="nomorHPSiswa" id="nomorHPSiswa" class="form-control" maxlength="12" value="<?=$data['siswa']['nomorHPSiswa'];?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="nomorHPBapak" class="col-md-3">Nomor HP Orang Tua</label>
    <div class="col-md-9">
      <input type="text" name="nomorHPBapak" id="nomorHPBapak" class="form-control" maxlength="12"  value="<?=$data['siswa']['nomorHPBapak'];?>">
    </div>
  </div>

  <div class="form-group d-flex justify-content-end">
    <input type="submit" value="Simpan" class="btn btn-primary">
  </div>

</form>
