<form class='form-horizontal' action="<?=$data['action'];?>" method="post">

  <div class="form-group row">
    <label for="mepelID" class="col-md-3">ID Mata Pelajaran</label>
    <div class="col-md-9">
      <input type="text" name="mapelID" id="mapelID" class="form-control" readonly value="<?=$data['mapel']['mapelID'];?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="proli" class="col-md-3">Program Keahlian</label>
    <div class="col-md-9">
      <select name="proli" id="proli" class="form-control">
        <option value="ALL" selected>Semua Program Keahlian</option>
        <option value="TKJ">Teknik Komputer dan Jaringan</option>
        <option value="TKR">Teknik Kendaraan Ringan</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="initMapel" class="col-md-3">Singkatan Mata Pelajaran</label>
    <div class="col-md-9">
      <input type="text" name="initMapel" id="initMapel" class="form-control" required maxlength=10  value="<?=$data['mapel']['initMapel'];?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="namaMapel" class="col-md-3">Nama Mata Pelajaran</label>
    <div class="col-md-9">
      <input type="text" name="namaMapel" id="namaMapel" class="form-control" required  value="<?=$data['mapel']['namaMapel'];?>">
    </div>
  </div>
  
  <div class="form-group d-flex justify-content-end">
    <input type="submit" value="Simpan" class="btn btn-primary">
  </div>
</form>
