
<form class='form-horizontal' action="<?=BASEURL;?>Manager/chgKontrak" method="post">
<input type="hidden" name="gmpkID" value="<?=$data['kontrak']['gmpkID'];?>">
<div class="form-group row">
  <label for="tapel" class="col-md-3">Tahun Pelajaran</label>
  <div class="col-md-9">
    <input type="text" name="tapel" id="tapel" class="form-control" required placeHolder="2020/2021" value="<?=$data['kontrak']['tapel'];?>">
  </div>
</div>

<div class="form-group row">
  <label for="mapelID" class="col-md-3">Mata Pelajaran</label>
  <div class="col-md-9">
    <select name="mapelID" id="mapelID" class="form-control">
      <option value="">Pilih Mata Pelajaran</option>
      <?php 
        foreach($data['mapel'] as $mapel): 
          $sel = $mapel['mapelID'] == $data['kontrak']['mapelID'] ? "selected" : "";
      ?>
      <option <?=$sel;?> value="<?=$mapel['mapelID'];?>"><?=$mapel['namaMapel'];?></option>
      <?php endforeach; ?>
    </select>
  </div>
</div>

<div class="form-group row">
  <label for="niy" class="col-md-3">Guru Pengajar</label>
  <div class="col-md-9">
    <select type="text" name="niy" id="niy" class="form-control">
      <option value="">Pilih Guru</option>
      <?php 
        foreach($data['guru'] as $guru): 
          $sel = $guru['niy'] == $data['kontrak']['niy'] ? "selected" : "";
      ?>
      <option <?=$sel;?> value="<?=$guru['niy'];?>"><?=$guru['namaLengkap'];?></option>
      <?php endforeach; ?>
    </select>
  </div>
</div>

<div class="form-group row">
  <label for="kelasID" class="col-md-3">Kelas Yang Diampu</label>
  <div class="col-lg-9">
      <select name="kelasID" id="kelasID" class="form-control">
      <?php 
      foreach($data['kelas'] as $kelas): 
        $sel = $kelas['kelasID'] == $data['kontrak']['kelasId'] ? "selected" : "";
      ?>
          <option <?=$sel;?> value="<?=$kelas['kelasID'];?>"><?=$kelas['tingkat'];?> <?=$kelas['proli'];?> <?=$kelas['ruang'];?></option>
      <?php endforeach; ?>
      </select>
  </div>
  <div class="col-md-9 d-flex flex-wrap">
  </div>
</div>

<div class="form-group d-flex justify-content-end">
  <input type="submit" value="Simpan" class="btn btn-primary">
</div>
</form>