    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>DAFTAR MATA PELAJARAN</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="<?=BASEURL;?>Manager/frmapel" class="btn btn-primary" >
                <i class="far fa-plus-square icon48"> Mapel</i>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <?php Alert::sankil(); ?>
            <table class="table table-sm-table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Singkatan</th>
                        <th>Nama Mapel</th>
                        <th>Program Keahlian</th>
                        <th class="text-center">
                            <i class="fas fa-gears"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data['mapel'] as $mapel ): ?>
                    <tr>
                        <td><?=$mapel['mapelID'];?></td>
                        <td><?=$mapel['initMapel'];?></td>
                        <td><?=$mapel['namaMapel'];?></td>
                        <td><?=$mapel['proli'];?></td>
                        <td class="text-center">
                            <a href="<?=BASEURL;?>Manager/frmapel/<?=$mapel['mapelID'];?>" class="mx-1">
                                <i class="fas fa-edit icon48"></i>
                            </a>

                            <a href="javascript:void(0)" class="rmmapel mx-1">
                                <i class="fas fa-times icon48"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div> <!-- conteiner-fluid -->

<?php $this->view('template/bs4js') ; ?>
<script>
$('.rmmapel').on('click' , function(){
    let mapelID = $(this).parent().parent().children('td:nth-child(1)').text();
    let tenan = confirm("Data akan dihapus!");
    if ( tenan == true ){
        $.post("<?=BASEURL;?>Manager/rmvMapel" , { mapelID:mapelID } , function(resp){
            if( resp == '1') location.reload();
        })
    }
})
</script>