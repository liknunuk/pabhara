<?php
function randomPassword(){
    $imak = mt_rand(0,9);
    $makanan=['pecel','rujak','soto','bakso','bakmi','rendang','cilok','cimol','siomai','seblak'];
    $itbc = mt_rand(0,5);
    $tandabaca = ['%','#','@','*','_'];
    $angka=mt_rand(601,999);
    return $makanan[$imak] . $tandabaca[$itbc] . $angka;
}
?>
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>LOGIN GURU KARYAWAN DAN SISWA</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <div class="form-horizontal">
                <div class="form-group row">
                    <label for="userID0" class="col-md-4">Masukkan NIY / NIS</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="userID0">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="namaLengkap" class="col-md-4">Nama Lengkap</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="namaLengkap" disabled>
                    </div>
                </div>
            </div>
            <form class="form-horizontal" method="post" action="<?=BASEURL;?>Manager/setUser">
                <input type="hidden" name="umodus" value="enggal">
                <div class="form-group row">
                    <label for="userType" name="usertype" class="col-md-4">Tipe User</label>
                    <div class="col-md-8">
                        <select class="form-control" id="userType" name="usertype">
                            <option value="niy">Guru</option>
                            <option value="nis">Siswa</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="userID" class="col-md-4">User ID</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="userID" name="userID" placeholder="HARUS SAMA DENGAN NIY ATAU NIS">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="userName" class="col-md-4">Username</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="userName" name="userName" placeholder="nama untuk login">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="userAuth" class="col-md-4">Kata Sandi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="userAuth" name="userAuth" value="<?=randomPassword();?>">
                    </div>
                </div>

                <div class="form-group d-flex px-2 justify-content-center">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div> <!-- conteiner-fluid -->

<?php $this->view('template/bs4js') ; ?>
<script>
$('#userID0').on('keypress',function(e){
    e.preventDefault();
    if(e.keyCode=='13'){
        $.ajax({
            method  : 'get',
            url     : '<?=BASEURL;?>Manager/userFind/'+$(this).val(),
            success : function(res){
                let data = JSON.parse(res), utype,uname;
                uname = data.userID.replace(/-/g,'');
                uname = uname.toLowerCase();
                $('#namaLengkap').val( data.namaLengkap);
                if( data.type == 'nis' ){ utype='Siswa'; }else{utype = 'Guru'; }
                $('#userType option').each( function(){
                    if( $(this).val() == data.type){
                        $(this).prop('selected',true);
                    }
                });
                $('#userID').val( data.userID );
                $('#userName').val(uname);
                $('input[name=umodus]').val('gantos');
            }
        })
    }
})
</script>