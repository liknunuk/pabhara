    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>DAFTAR SISWA</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="<?=BASEURL;?>Manager/frsiswa" class="btn btn-primary" >
                <i class="far fa-plus-square icon48"> Siswa</i> 
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <?php Alert::sankil(); ?>
            <table class="table table-sm table-striped table-bordered">
                <thead>
                    <tr>
                        <th>NIPD</th>
                        <th>Nama Lengkap</th>
                        <th>Jenis Kelamin</th>
                        <th>Nama Orang Tua</th>
                        <th>No. HP Siswa</th>
                        <th>No. HP Orang Tua</th>
                        <th class="text-center">
                            <i class="fas fa-gears"></i>
                        </th>
                    </tr>
                </thead>
                <tbody id="loSiswa">
                    <?php foreach($data['siswa'] as $siswa): ?>
                    <tr>
                        <td><?=$siswa['nis'];?></td>
                        <td><?=$siswa['namaLengkap'];?></td>
                        <td><?=$siswa['jenisKelamin'];?></td>
                        <td><?=$siswa['namaAyah'];?></td>
                        <td><?=$siswa['nomorHPSiswa'];?></td>
                        <td><?=$siswa['nomorHPBapak'];?></td>
                        <td class="text-center">
                            <a href="<?=BASEURL;?>Manager/frsiswa/<?=$siswa['nis'];?>">
                            <i class="fas fa-edit icon48"></i>
                            </a>
                            <a href="javascript:void(0)" onClick=fired("<?=$siswa['nis'];?>")>
                            <i class="fas fa-times icon48"></i>
                            </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php $this->view('home/pagina',$data);?>
</div> <!-- conteiner-fluid -->

<?php $this->view('template/bs4js') ; ?>
<script>
    function fired($nis){
        let tenan = confirm('Data akan dihapus!');
        if ( tenan == true ){
            $.post("<?=BASEURL;?>Manager/droppingout",{nis:$nis}, function(resp){
                if( resp == '1' ){
                    location.reload();
                }
            })
        }
    }

    $('#cari').click( function(){
        let katakunci = $('#katakunci').val();
        $.ajax({
            dataType: 'json',
            method: 'POST',
            url: "<?=BASEURL;?>Manager/siswabyname",
            data: { nama:katakunci },
            success: function(resp){
                $("#loSiswa tr").remove();
                $.each( resp , function(i,data){
                    $('#loSiswa').append(`
                    <tr>
                    <td>${data.nis}</td>
                    <td>${data.namaLengkap}</td>
                    <td>${data.jenisKelamin}</td>
                    <td>${data.namaAyah}</td>
                    <td>${data.nomorHPSiswa}</td>
                    <td>${data.nomorHPBapak}</td>
                    <td class="text-center">
                    <a href="<?=BASEURL;?>Manager/frsiswa/${data.nis}">
                    <i class="fas fa-edit icon48"></i>
                    </a>
                    <a href="javascript:void(0)" onClick=fired("${data.nis}")>
                    <i class="fas fa-times icon48"></i>
                    </a>
                    </td>
                    </tr>
                    `)
                })
            }
        })
    })
</script>