    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Daftar Guru</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="<?=BASEURL;?>Manager/frguru" class='btn btn-primary'>
                <i class="far fa-plus-square icon48"> Guru</i>
            </a>
            <div class="table-responsive">
                <?php Alert::sankil(); ?>
                <table class="table table-sm table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>NIY</th>
                            <th>NAMA LENGKAP</th>
                            <th>JENIS KELAMIN</th>
                            <th>NOMOR HP</th>
                            <th>KETERANGAN</th>
                            <th class='text-center'><i class="fas fa-gears"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['guru'] as $gk): ?>
                        <tr>
                            <td><?=$gk['niy'];?></td>
                            <td><?=$gk['namaLengkap'];?></td>
                            <td><?=$gk['jenisKelamin'];?></td>
                            <td><?=$gk['nomorHP'];?></td>
                            <td><?=$gk['posisi'];?></td>
                            <td class='text-center'>
                                <a href="<?=BASEURL;?>Manager/frguru/<?=$gk['niy'];?>">
                                    <i class="fas fa-edit mx-1"></i>
                                </a>
                                <a href="javascript:void(0)" onclick=fired("<?=$gk['niy'];?>")>
                                    <i class="fas fa-times mx-1"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php $this->view('home/pagina',$data) ;?>
</div> <!-- conteiner-fluid -->

<?php $this->view('template/bs4js') ; ?>
<script>
    function fired($niy){
        let tenan = confirm('Data akan dihapus!');
        if ( tenan == true ){
            $.post("<?=BASEURL;?>Manager/firing",{niy:$niy}, function(resp){
                if( resp == '1' ){
                    location.reload();
                }
            })
        }
    }
</script>
