    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>PANDEL DATA SMK PANCA BHAKTI RAKIT BANJARNEGARA</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h4 class="mb-3 text-center"><strong>DATA SISWA</strong></h4>
        </div>
    </div>
    <div class="row">
        <!-- pandl data siswa -->
        <div class="col-lg-6">
            <div class="table-responsive">
                
                <table class="table table-sm table-bordered">
                    <thead>
                        <tr>
                            <th>Tingkat</th>
                            <th>Proli</th>
                            <th>Kelas</th>
                            <th>L/P</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $jml['L']=0; $jml['P']=0;
                    $jmt=[]; $jmp=[]; $jtp=[];
                    ?>
                    <?php foreach($data['siswa'] as $siswa ): ?>
                        <tr>
                            <td><?=$siswa['tingkat'];?></td>
                            <td><?=$siswa['proli'];?></td>
                            <td><?=$siswa['kelasID'];?></td>
                            <td><?=$siswa['jenisKelamin'];?></td>
                            <td class="text-right"><?=$siswa['jumlah'];?></td>
                        </tr>
                    <?php
                    $jkl = $siswa['jenisKelamin'];
                    $tkt = $siswa['tingkat'];
                    $prl = $siswa['proli'];
                    $jml[$jkl] +=$siswa['jumlah'];                    
                    $jmt[$tkt][$jkl] += $siswa['jumlah'];
                    $jmp[$prl][$jkl] += $siswa['jumlah'];
                    $jtp[$tkt][$prl][$jkl]+=$siswa['jumlah'];
                    endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-6">
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <th>Total Jumlah Siswa </th>
                    <td class='text-right'><?=$jml['L'] + $jml['P'];?> Siswa</td>
                </tr>
                <tr>
                    <th>Total Jumlah Siswa Laki-laki</th>
                    <td class='text-right'><?=$jml['L'];?> Siswa</td>
                </tr>
                <tr>
                    <th>Total Jumlah Siswa Perempuan</th>
                    <td class='text-right'><?=$jml['P'];?> Siswa</td>
                </tr>
                <?php 
                foreach($jmt as $tkt=>$stk): 
                    foreach($stk as $jk=>$jml):
                        $jn = $jk=="L" ? "Laki-laki" : "Perempuan";
                ?>
                <tr>
                    <th>Jumlah Siswa Tingkat <?=$tkt;?> <?=$jn;?></th>
                    <td class="text-right"><?=$jml;?> Siswa</td>
                </tr>
                <?php 
                    endforeach; 
                endforeach; 
                foreach($jmp as $pro=>$siswa):
                    foreach($siswa as $jk=>$jml):
                        $jn = $jk=="L" ? "Laki-laki" : "Perempuan";
                ?>

                <tr>
                    <th>Jumlah Siswa Program Keahlian <?=$pro;?> <?=$jn;?></th>
                    <td class="text-right"><?=$jml;?> Siswa</td>
                </tr>
                <?php
                    endforeach;
                endforeach;
                // $jtp[$tkt][$prl][$jkl]+=$siswa['jumlah'];
                foreach($jtp as $tkt=>$spk):
                    foreach($spk as $pro=>$jms):
                        foreach($jms as $jk=>$jml):
                            $jn = $jk=="L" ? "Laki-laki" : "Perempuan";
                ?>
                <tr>
                    <th>Jumlah Siswa Tingkat <?=$tkt?> Program Keahlian <?=$pro;?> <?=$jn;?></th>
                    <td class="text-right"><?=$jml;?> Siswa</td>
                </tr>
                <?php 
                        endforeach;
                    endforeach;
                endforeach;
                ?>
            </tbody>
        </table>
        </div>
    </div>
</div> <!-- conteiner-fluid -->

<?php $this->view('template/bs4js') ; ?>
<script>
</script>