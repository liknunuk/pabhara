    <div class="row d-flex justify-content-end">
        <div class="col-lg-4">
            <div class="form-group row">
                <label for="kelasID" class="col-md-4">Pilih Kelas</label>
                <div class="col-md-8">
                    <select id="kelasID" class="form-control">
                        <option value="">Pilih Kelas</option>
                        <?php foreach($data['kelas'] as $kelas):?>
                        <option value="<?=$kelas['kelasID'];?>">Kelas <?="{$kelas['tingkat']} {$kelas['proli']} {$kelas['ruang']}";?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Daftar Siswa Kelas <?=$data['kelasiswa']['tingkat'];?> <?=$data['kelasiswa']['proli'];?> <?=$data['kelasiswa']['ruang'];?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 table-responsive mx-auto">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>NIS</th>
                        <th>No. Urut</th>
                        <th>Nama Lengkap</th>
                        <th>Jenis Kelamin</th>
                        <th class="text-center">
                            <i class="fas fa-gears"></i>
                        </th>
                    </tr>
                </thead>
                <tbody id="loSiswa">
                <?php foreach($data['siswa'] as $siswa): ?>
                    <tr>
                        <td><?=$siswa['nis'];?></td>
                        <td><?=$siswa['nmAbsen'];?></td>
                        <td><?=$siswa['namaLengkap'];?></td>
                        <td><?=$siswa['jenisKelamin'];?></td>
                        <td class='text-center'><i class='fas fa-times'></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
</div> <!-- conteiner-fluid -->

<?php $this->view('template/bs4js') ; ?>
<script>
$("#kelasID").change(function(){
    let kelasID=this.value;
    // alert(kelasID);
    $.ajax({
        dataType:'json',
        url: "<?=BASEURL;?>Home/siswaKelas/"+kelasID,
        success: function(resp){
            $("#loSiswa tr").remove();
            $.each( resp , function(i,data){
                $("#loSiswa").append(`
                <tr>
                <td>${data.nis}</td>
                <td>${data.nmAbsen}</td>
                <td>${data.namaLengkap}</td>
                <td>${data.jenisKelamin}</td>
                <td class='text-center'><i class='fas fa-times'></td>
                </tr>
                `)
            })
        }
    })
})
</script>