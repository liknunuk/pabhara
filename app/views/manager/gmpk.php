    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>KONTRAK MENGAJAR</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="<?=BASEURL;?>Manager/frAddKontrak" class="btn btn-primary">
                <i class="far fa-plus-square icon48"> Kontrak</i>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mt-3 table-responsive">
            <table class="table table-sm table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No. Kontrak</th>
                        <th>Tahun Pelajaran</th>
                        <th>Guru Pengajar</th>
                        <th>Mata Pelajaran</th>
                        <th>Kelas</th>
                        <th class="text-center">
                            <i class="fas fa-gears"></i>
                        </th>
                    </tr>
                </thead>
                <tbody id="loKontrak">
                <?php foreach($data['kontrak'] as $kontrak): ?>
                    <tr>
                        <td><?=$kontrak['gmpkID'];?></td>
                        <td><?=$kontrak['tapel'];?></td>
                        <td><?=$kontrak['namaLengkap'];?></td>
                        <td><?=$kontrak['namaMapel'];?></td>
                        <td><?=$kontrak['tingkat'];?> <?=$kontrak['proli'];?>-<?=$kontrak['ruang'];?></td>
                        <td class="text-center">
                            <a href="<?=BASEURL;?>Manager/frChgKontrak/<?=$kontrak['gmpkID'];?>" class="mx-1">
                                <i class="fas fa-edit icon48"></i>
                            </a>

                            <a href="" class="mx-1 rmvGmpk">
                                <i class="fas fa-times icon48" style="color:red;"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div> <!-- row -->
    <?php $this->view('home/pagina',$data); ?>
    
</div> <!-- conteiner-fluid -->

<?php $this->view('template/bs4js') ; ?>
<script>
$(".rmvGmpk").click( function(){
    let gmpkID = $(this).parent().parent().children('td:nth-child(1)').text();
    let tenan = confirm("Data akan dihapus!");
    if ( tenan == true ){
        $.post("<?=BASEURL;?>/Manager/putusKontrak" , { gmpkID:gmpkID }, function(resp){
            if(resp == '1' ){
                location.reload();
            }
        })
    }
})
</script>