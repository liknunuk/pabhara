    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>DATA RUANG KELAS</h1>
        </div>
    </div>
    <!--
    <div class="row">
        <div class="col-lg-12">
            <a href="<?=BASEURL;?>Manager/frkelas" class="btn btn-primary">
                <i class="far fa-plus-square icon48"> Kelas</i>
            </a>
        </div>
    </div>
    -->
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-sm table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Kelas ID</th>
                        <th>Tingkat</th>
                        <th>Program</th>
                        <th>Urut Ruang</th>
                        <th>Status Kelas</th>
                        <th class="text-center">
                            <i class="fas fa-gears icon48"></i>
                        </th>
                    </tr>
                </thead>
                <tbody id="loKelas">
                <?php foreach($data['kelas'] as $kelas): ?>
                    <tr <?php if($kelas['statuskelas']=='Inaktif') echo "class='text-secondary'";?> >
                        <td><?=$kelas['kelasID'];?></td>
                        <td><?=$kelas['tingkat'];?></td>
                        <td><?=$kelas['proli'];?></td>
                        <td><?=$kelas['ruang'];?></td>
                        <td><?=$kelas['statuskelas'];?></td>
                        <td class="text-center">
                            <?php if($kelas['statuskelas']=="Aktif"): ?>
                            <a href="javascript:void(0)" class="kelasOF">
                                <i class="fas fa-toggle-off" style="font-size: 24px; color:red;"></i>
                            </a>
                            <?php else: ?>
                            <a href="javascript:void(0)" class="kelasON">
                                <i class="fas fa-toggle-on" style="font-size: 24px; color:blue;"></i>
                            </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div> <!-- conteiner-fluid -->

<?php $this->view('template/bs4js') ; ?>
<script>
    $('.kelasOF').on('click',function(){
        let kelasID = $(this).parent().parent().children('td:nth-child(1)').text();
        $.post('<?=BASEURL;?>Manager/setActivity', 
        { statuskelas:'Inaktif' , kelasID:kelasID },
        function(resp){
            if(resp == "1"){
                location.reload();
            }
        })
    })

    $('.kelasON').on('click',function(){
        let kelasID = $(this).parent().parent().children('td:nth-child(1)').text();
        $.post('<?=BASEURL;?>Manager/setActivity', 
        { statuskelas:'Aktif' , kelasID:kelasID },
        function(resp){
            if(resp == "1"){
                location.reload();
            }
        })
    })
</script>