<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        
        <ul class="list-inline">
            <li class="list-inline-item siswaProperties"><?=$data['siswa']['nis'];?></li>
            <li class="list-inline-item siswaProperties"><?=$data['siswa']['namaLengkap'];?></li>
            <li class="list-inline-item siswaProperties"><?=$data['kelas']['tapel'];?></li>
            <li class="list-inline-item siswaProperties"><?=$data['kelas']['kelasID'];?></li>
            <li class="list-inline-item siswaProperties"><?=$data['kelas']['nmAbsen'];?></li>
            <li class="list-inline-item siswaProperties"><?=$data['siswa']['statusSiswa'];?></li>
            <li class="list-inline-item siswaProperties">
                <a href="<?=BASEURL;?>Home/logout" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i></a>
            </li>
        </ul>
        </div>
    </div>
    <div class="row">
        <?php foreach($data['jurnal'] as $jurnal): ?>
            <div class="col-sm-4">
                <!-- Array ( [topic] => Teknik Lempar Batu [materiPbm] => [materiEvaluasi] => [tugasMandiri] => [namaLengkap] => Saefulloh [kelasId] => 10TKR01 ) -->
                <div class="list-group border border-dark mt-3">
                    <div class="list-group-item py-1"><?=$jurnal['kelasId'];?></div>
                    <div class="list-group-item py-1"><?=$jurnal['namaMapel'];?></div>
                    <div class="list-group-item py-1"><?=$jurnal['namaLengkap'];?></div>
                    <div class="list-group-item py-1"><?=$jurnal['topic'];?></div>
                    <div class="list-group-item py-1"><a href="<?=$jurnal['materiPbm'];?>">Bahan Bacaan</a></div>
                    <div class="list-group-item py-1"><a href="<?=$jurnal['tugasMandiri'];?>">Tugas Mandiri</a></div>
                    <div class="list-group-item py-1"><a href="<?=$jurnal['materiEvaluasi'];?>">Evaluasi</a></div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div> <!-- container -->
<?php $this->view('template/bs4js'); ?>
<script>
</script>