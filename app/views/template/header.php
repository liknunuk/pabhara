<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?= webtitle; ?></title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="https://nugrahamedia.web.id/nugrahamedia.png" width="16px" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap 4 CDN -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- jquery-ui -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://kit.fontawesome.com/8b0fcf1687.js" crossorigin="anonymous"></script>
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= BASEURL .'css/pbrakit.css'; ?>">
  <!-- font google -->
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Bree+Serif&family=Fraunces&family=Yanone+Kaffeesatz:wght@300&family=Zilla+Slab:wght@400;500&display=swap');
  </style>
</head>
<body>
