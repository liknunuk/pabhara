<?php
    if($data['pn']==1){
        $prev=1;
        $next=2;
    }else{
        $prev=$data['pn']-1;
        $next=$data['pn']+1;
    }
?>

<div class="row">
    <div class="col-md-4 mx-auto text-center">
        <a href="<?=$data['url'];?>/<?=$prev;?>">
        <i class="far fa-caret-square-left text-primary" style="font-size: 24px;"></i>
    </a>
    <span class="mx-2"><?=$data['pn'];?></span>
    <a href="<?=$data['url'];?>/<?=$next;?>">
        <i class="far fa-caret-square-right text-primary" style="font-size: 24px;"></i>
    </a>
    </div>
</div>