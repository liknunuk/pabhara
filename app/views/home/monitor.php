<div class="container-fluid">
  <div class="row h-100">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
            <h3>Tingkat Kehadiran Siswa</h3>
            <p>Tanggal : <?=date('d F Y');?></p>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Keterangan</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['presensi'] as $presensi):?>
                        <tr>
                            <td><?=$presensi['keterangan'];?></td>
                            <td class="text-right px-2"><?=$presensi['jumlah'];?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
            <h3>Kontrol Jurnal PBM</h3>
            <p>Tanggal : <?=date('d F Y');?></p>
            </div>
            <div class="card-body">
                <ul class="list-group">
                <!-- // jpbmID , waktuCatat , topic , namaMapel , namaLengkap , kelas -->
                <?php foreach($data['jurnal'] as $jurnal): ?>
                    <li class="list-group-item py-0 mb-1">
                        <p class="my-0"><i><?=$jurnal['waktuCatat'];?> - <?=$jurnal['kelas'];?></i> - <?=$jurnal['namaLengkap'];?></p>
                        <p class="my-0"><?=$jurnal['namaMapel'];?>. Topic: <?=$jurnal['topic'];?> </p>                        
                    </li>
                <?php endforeach; ?>
                </ul>
            
            </div>
        </div>    
    </div>
    
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
