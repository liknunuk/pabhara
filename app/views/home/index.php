<div class="container-fluid h-100 loginpage">
  <div class="row h-100">
    <div class="col-sm-4 my-auto py-2 mx-auto">
      <div class="card logincard">
          <div class="card-header text-center bg-secondary">
          <h4 class="cardheader">Login Pengguna</h4>
          </div>

          <div class="card-body">
            <form action="<?=BASEURL;?>Home/userAuth" method="post">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="login-user"><i class="fas fa-user-tie" style="font-size: 24px;"></i></span>
                    </div>
                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="login-user" name ="userName" value="gty210022">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="login-pass"><i class="fas fa-unlock-alt" style="font-size: 24px;"></i></span>
                    </div>
                <input type="password" class="form-control" placeholder="Kata Sandi" aria-label="Kata Sandi" aria-describedby="login-pass" name="userAuth" value="gurupabhara">
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success w-50">LogiN</button>
                </div>

            </form>

          </div>
      </div>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
