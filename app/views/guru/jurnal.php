    <div class="row">
        <div class="col-lg-12 font-weight-bold text-center">
        <h4>Jurnal Mengajar Tahun Pelajaran <?=tapel;?><br/><?=$data['kontrak']['namaMapel'];?><br/>Kelas <?=$data['kontrak']['tingkat'];?> <?=$data['kontrak']['proli'];?><?=$data['kontrak']['ruang'];?></h4>        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="jurnal-result text-center d-flex">
                <div>
                    <?php if(empty($data['jurnal'])){ echo "Belum Ada Jurnal"; }else{ echo "Terdapat Total ".count($data['jurnal']) . " jurnal"; } ?>
                </div>
                    <a href="javascript:void(0)" id="<?=$data['kontrak']['gmpkID'];?>" class="btn btn-primary ml-auto"><i class="fas fa-plus"> Jurnal Baru</i></a>
            </div>
        </div>
        
    </div>
    <div class="row" id="frJurnal">
        <div class="col-lg-12">
            
            <form action="<?=BASEURL;?>Guru/jurnalbaru" method="post">
                <input type="hidden" name="mod" value="<?=$data['fmod'];?>">

                <div class="form-group">
                    <label>Jurnal ID</label>
                    <?php if($data['fmod'] == 'new') {
                        $jurnalID = $data['jurnalIDbaru'];
                    }else{
                        $jurnalID = $data['jpbm']['jpbmID'];
                    }
                    ?>
                    <input type="text" name="jpbmID" id="jpbmID" class="form-control" readonly 
                    value="<?=$jurnalID;?>">
                </div>

                <div class="form-group">
                    <?php
                        $smt = date('m') > 6 ? 1 : 2;
                    ?>
                    <label>Semester</label>
                    <input type="number" name="semester" id="semester" class="form-control" placeholder="Materi pembelajaran" min=1 max=6 value="<?php if($data['fmod']=='new'){ echo $smt; }else{ echo $data['jpbm']['semester'];} ?>">
                </div>

                <div class="form-group">
                    <label>Topik</label>
                    <input type="text" name="topic" id="topic" class="form-control" placeholder="Topik pembelajaran" required value="<?=$data['jpbm']['topic'];?>">
                </div>

                <div class="form-group">
                    <label>Materi Pembelajaran</label>
                    <input type="text" name="materiPbm" id="materiPbm" class="form-control" placeholder="URL Bahan Pembelajaran" value="<?=$data['jpbm']['materiPbm'];?>">
                </div>

                <div class="form-group">
                    <label>Tugas Mandiri</label>
                    <input type="text" name="tugasMandiri" id="tugasMandiri" class="form-control" placeholder="URL Bahan Tugas Mandiri" value="<?=$data['jpbm']['tugasMandiri'];?>">
                </div>

                <div class="form-group">
                    <label>Evaluasi</label>
                    <input type="text" name="materiEvaluasi" id="materiEvaluasi" class="form-control" placeholder="URL Bahan Evaluasi" value="<?=$data['jpbm']['materiEvaluasi'];?>">
                </div>

                <div class="form-group d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary">Catat Jurnal!</button>
                </div>

            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12"><?php Alert::sankil(); ?></div>
    </div>

    <!-- list of 60 last journal -->
    <div class="row">
        <?php 
            foreach($data['jurnal'] as $jg): 
                $jid = substr($data['jurnalIDbaru'],7,4);
                $linkPbm = $jg['materiPbm']== NULL ? BASEURL . "Guru/jurnal/{$jid}/#" : $jg['materiPbm'];
                $linkTgm = $jg['tugasmandiri']== NULL ? BASEURL . "Guru/jurnal/{$jid}/#" : $jg['tugasMandiri'];
                $linkEva = $jg['materiEvaluasi']== NULL ? BASEURL . "Guru/jurnal/{$jid}/#" : $jg['materiEvaluasi'];
        ?>
        <div class="col-sm-3">
            <ul class="list-group">
                <li class="list-group-item list-group-item-sm bg-info"><?=$jg['jpbmID'];?><span class="float-right"><a href="<?=BASEURL?>Guru/Jurnal/<?=$jid;?>/<?=$jg['jpbmID'];?>"><i class="fas fa-edit"></i></a></span>
                </li>
                <li class="list-group-item py-1">Tanggal: <?=$jg['waktuCatat'];?></li>
                <li class="list-group-item py-1">Topik: <?=$jg['topic'];?></li>
                <li class="list-group-item py-1"><a href="<?=$linkPbm;?>">Bahan Bacaan</a></li>
                <li class="list-group-item py-1"><a href="<?=$linkTgm;?>">Tugas Mandiri</a></li>
                <li class="list-group-item py-1"><a href="<?=$linkEva;?>">Bahan Evaluasi</a></li>
                <li class="list-group-item py-1">Catatan:<br/>
                <?php if($jg['catatan']==NULL): ?>
                <button class="btn btn-info catatan">Buat Catatan</button>
                <?php else: ?>
                <button class="btn btn-info catatan">Ganti Catatan</button><br/>
                <?php echo $jg['catatan']; endif; ?>
                </li>
                <li class="list-group-item py-1 text-center">
                    <a href="<?=BASEURL;?>Guru/presensijurnal/<?=$jg['jpbmID'];?>" class="btn btn-primary">Kehadiran Siswa</a>                    
                </li>
            </ul>
        </div>
        <?php endforeach; ?>
    </div>
</div> <!-- container -->
<!-- modals -->
<!-- modal catatan jurnal -->
<div class="modal" tabindex="-1" role="dialog" id="mdCatatanJurnal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Catatan Jurnal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="<?=BASEURL;?>Guru/addNote" method="post">
       <input type="hidden" name="jpbmID" id="mdcjJpbmID">
       <input type="hidden" name="gmpkID" id="mdcjGmpkID">
       <div class="form-group">
        <label for="catatan">Catatan</label>
        <textarea name="catatan" id="catatan" rows="5" class="form-control"></textarea>
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="reset" class="btn btn-primary">Ganti</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
       </form>
      </div>
    </div>
  </div>
</div>
<!-- modals -->
<?php $this->view('template/bs4js'); ?>
<script>
    $(".catatan").click( function(){
        let jpbmID = $(this).parent().parent().children('li:nth-child(1)').text();
        $('#mdCatatanJurnal').modal('show');
        $("#mdcjJpbmID").val(jpbmID);
        $("#mdcjGmpkID").val("<?=$data['kontrak']['gmpkID'];?>");
        
    })
</script>