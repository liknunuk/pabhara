    <div class="row">
        <div class="col-lg-12 font-weight-bold text-center">
        <h4>Kehadiran Siswa</h4>        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
       
        <table class="table table-sm">
            <tbody>
                <tr>
                    <td>Jurnal ID</td>
                    <td><?=$data['jurnal']['jpbmID'];?></td>
                </tr>
                <tr>
                    <td>Mata Pelajaran</td>
                    <td><?=$data['jurnal']['namaMapel'];?></td>
                </tr>
                <tr>
                    <td>Semester</td>
                    <td><?=$data['jurnal']['semester'];?></td>
                </tr>
                <tr>
                    <td>Topic</td>
                    <td><?=$data['jurnal']['topic'];?></td>
                </tr>
                <tr>
                    <td>Kelas</td>
                    <td><?=$data['jurnal']['kelas'];?></td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>NIS</th>
                        <th>No. Urut</th>
                        <th>Nama Lengkap</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                <?php $nmr=1; foreach($data['siswa'] as $siswa): ?>
                    <tr>
                        <td><?=substr($siswa['presenceID'],-9);?></td>
                        <td><?=$nmr;?>.</td>
                        <td><?=$siswa['namaLengkap'];?></td>
                        <td><?=$siswa['keterangan'];?></td>
                    </tr>
                <?php $nmr++; endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
</div> <!-- container -->
<?php $this->view('template/bs4js'); ?>
<script>
</script>