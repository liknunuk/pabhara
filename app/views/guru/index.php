    <div class="row">
        <div class="col-lg-12 py-2"><h4 class="text-center">Tahun Pelajaran <?=tapel;?></h4></div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header"><h3>Kontrak Mengajar</h3></div>
                <div class="card-body">
                <?php foreach($data['kontrak'] as $kontrak): ?>
                    <div class="d-flex">
                        <div class="guru-list">
                            <?=$kontrak['namaMapel'] . " - " . $kontrak['tingkat'] . " " . $kontrak['proli'] . " " . $kontrak['ruang'];?>
                        </div>
                        <div class="ml-auto">
                            <a href="<?=BASEURL;?>Guru/jurnal/<?=$kontrak['gmpkID'];?>" class="btn btn-success">
                                <i class="fas fa-file-alt" style="font-size: 48px;"></i>
                            </a>
                        </div>
                    </div>                    
                <?php endforeach; ?>
                </div>
            </div>        
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header"><h3>Jurnal Mengajar</h3></div>
                <div class="card-body px-0 py-0">
                    <div class="list-group">
                    <?php foreach($data['jurnal'] as $jurnal):?>
                        <li class="list-group-item px-1 mb-2" style="font-size:20px;">
                            <div class="py-0 my-0"><?=$jurnal['waktuCatat'];?> Kelas <?=$jurnal['kelas'];?></div>
                            <div class="py-0 my-0"><?=$jurnal['namaMapel'];?> Semester <?=$jurnal['semester'];?></div>
                            <div class="py-0 my-0"><i><?=$jurnal['topic'];?></i></div>
                        </li>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>        
        </div>
    </div>
</div> <!-- container -->
<?php $this->view('template/bs4js'); ?>
<script>
</script>