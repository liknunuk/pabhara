    <div class="row">
        <div class="col-lg-12 font-weight-bold text-center">
        <h4>Kehadiran Siswa</h4>        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
       
            <table class="table table-sm">
                <tbody>
                    <tr>
                        <td>Jurnal ID</td>
                        <td><?=$data['jurnal']['jpbmID'];?></td>
                    </tr>
                    <tr>
                        <td>Mata Pelajaran</td>
                        <td><?=$data['jurnal']['namaMapel'];?></td>
                    </tr>
                    <tr>
                        <td>Semester</td>
                        <td><?=$data['jurnal']['semester'];?></td>
                    </tr>
                    <tr>
                        <td>Topic</td>
                        <td><?=$data['jurnal']['topic'];?></td>
                    </tr>
                    <tr>
                        <td>Kelas</td>
                        <td><?=$data['jurnal']['kelas'];?></td>
                    </tr>
                </tbody>
            </table>        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form action="<?=BASEURL;?>Guru/setPresensi/<?=$data['jurnal']['kelasId'];?>" method="post">
            
            <div class="list-group">
            <?php 
                // $ket ='';
                foreach($data['siswa'] as $siswa): 
                    if( $data['preskelas'] == NULL ){
                        $ket='hadir';
                    }else{
                        $nis= $siswa['nis'];
                        $ket=$data['preskelas'][$nis];
                    }                    
            ?>
                <li class="list-group-item text-center">
                    <span>
                        <?=$siswa['namaLengkap'];?>
                    </span><br/>
                    <div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="<?=$data['jurnal']['jpbmID'];?>-<?=$siswa['nis'];?>" id="h_<?=$siswa['nis'];?>" value="hadir" <?php if($ket=='hadir') echo "checked"; ?> >
                            <label class="form-check-label" for="h_<?=$siswa['nis'];?>">Hadir </label>
                        </div>
                        
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="<?=$data['jurnal']['jpbmID'];?>-<?=$siswa['nis'];?>" id="s_<?=$siswa['nis'];?>" value="sakit" <?php if($ket=='sakit') echo "checked"; ?> >
                            <label class="form-check-label" for="s_<?=$siswa['nis'];?>">Sakit </label>
                        </div>
                        
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="<?=$data['jurnal']['jpbmID'];?>-<?=$siswa['nis'];?>" id="i_<?=$siswa['nis'];?>" value="izin" <?php if($ket=='izin') echo "checked"; ?> >
                            <label class="form-check-label" for="i_<?=$siswa['nis'];?>">Izin </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="<?=$data['jurnal']['jpbmID'];?>-<?=$siswa['nis'];?>" id="b_<?=$siswa['nis'];?>" value="bolos" <?php if($ket=='bolos') echo "checked"; ?> >
                            <label class="form-check-label" for="b_<?=$siswa['nis'];?>">Bolos </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="<?=$data['jurnal']['jpbmID'];?>-<?=$siswa['nis'];?>" id="k_<?=$siswa['nis'];?>" value="kabur" <?php if($ket=='kabur') echo "checked"; ?> >
                            <label class="form-check-label" for="k_<?=$siswa['nis'];?>">Minggat </label>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
                <li class="list-group-item text-center">
                    <button type="submit" class="btn btn-success">Catat</button>
                </li>
            </div>
            </form>
        </div>
    </div>
</div> <!-- container -->
<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready( function(){
    $.ajax({
        url:'<?=BASEURL;?>Guru/daypres/<?=$data['jurnal']['kelasId'];?>',
        success: function(resp){
            $("#presensiHarian").html(resp);
        }
    })
})
</script>